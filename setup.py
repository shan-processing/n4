# -*- coding: utf-8 -*-

from distutils.core import setup
from glob import glob
import subprocess

scripts = glob('scripts/*')
command = ['git', 'describe', '--tags']
version = subprocess.check_output(command).decode().strip()

setup(name='n4',
      version=version,
      description='Perform N4 bias correction for cerebellum MR image',
      author='Shuo Han',
      author_email='shan50@jhu.edu',
      install_requires=['nibabel', 'scipy', 'numpy'],
      scripts=scripts)
