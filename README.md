# N4

Perform N4 for cerebellum image. This packages uses ROBEX to get a brain mask, and the mask is smoothed as the weight in the N4 bias correction. N4 is perfomed by N4BiasFieldCorrection.

### Installation

1. `git clone <repo>`
2. Install [ROBEX](https://www.nitrc.org/projects/robex), and [ANTs](http://stnava.github.io/ANTs/)
3. Make sure `runROBEX.sh` from ROBEX and `N4BiasFieldCorrection` from ANTs are in your $PATH

### Example

```bash
perform_n4.sh -i input_image.nii.gz -o output_directory
```

Check `./perform_n4.sh -h` for more details
